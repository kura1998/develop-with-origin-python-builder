# develop-with-origin-python-builder

## command 👍  
```
$ cd src/main
$ python main.py 
```


## インストール
* [Graphviz.](http://www.graphviz.org/download/)
    * graphviz.py(ex:  ___\Anaconda3\envs\dod\Lib\site-packages\pydotplus)
    ![image](./image/path.png)
* [Pyyaml](https://anaconda.org/anaconda/pyyaml)
* [Scikit-learn](https://anaconda.org/anaconda/scikit-learn)
* [Pandas](https://anaconda.org/anaconda/pandas)
### メモ🐧
 `result = clf.feature_importances_ `
