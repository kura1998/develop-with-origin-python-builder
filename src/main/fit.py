from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree

import numpy as np
import pydotplus as pdp

clf = RandomForestClassifier()
label_num = 12
data_num = 2278
col_name = []
class_name = []

for i in range(label_num):
    class_name.append(str(i)) 
for i in range(int(data_num/2)):
    col_name.append('x' + str(i*2))
    col_name.append('y' + str(i*2+1))
print(len(col_name))

def  random_forest_fit(x_data, y_data):
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.33, random_state=42)    
    clf.fit(np.array(x_train), y_train)
    print('Score', clf.score(x_test, y_test))

def outTree():
    estimators = clf.estimators_
    file_name = "../resources/tree/tree_result.png"
    dot_data = tree.export_graphviz(estimators[0], out_file=None, filled=True,
                                    rounded=True, feature_names=col_name,
                                    class_names=class_name, special_characters=True)
    graph = pdp.graph_from_dot_data(dot_data)
    graph.write_png(file_name)