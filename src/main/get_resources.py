import pandas as pd, numpy as np, os


def read_csv(path, header=None):
    try :
        csv = pd.read_csv(path,  header=None)
        if len(csv.columns)  < 4:
            return None
        return csv.drop([0, 1, len(csv.columns) - 1], axis='columns')
    except:
        return None

def get_vector():
    directorys = os.listdir('../resources/csv/')
    x_data = []
    y_data = []
    euclidean = lambda arr: [np.sqrt(x*x + y*y) for x, y in arr] 
    for directory in directorys:
        try:
            li = read_csv('../resources/csv/' + directory).values
            x_data.append(euclidean(np.array(li)))
            if 'hiroya' in directory:
                y_data.append(0)
            elif 'satoru' in directory:
                y_data.append(1)
            elif 'tamu' in directory:
                y_data.append(2)
            elif 'tsubasa' in directory:
                y_data.append(3)
            elif 'yuki' in directory:
                y_data.append(4)
            elif 'hori' in directory:
                y_data.append(5)
            elif 'kazu' in directory:
                y_data.append(6)
            elif 'kudo' in directory:
                y_data.append(7)
            elif 'narita' in directory:
                y_data.append(8)
            elif 'ren' in directory:
                y_data.append(9)
            elif 'shoto' in directory:
                y_data.append(10)
            elif 'soichi' in directory:
                y_data.append(11)
            elif 'kanta' in directory:
                y_data.append(12)
        except:
            pass
    print('data length is', len(x_data))
    print('label length is', len(y_data))
    return x_data, y_data