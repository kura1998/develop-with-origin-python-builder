import os, sys, subprocess, yaml
settings = None
try:
	yamlFile = open("settings.yml", "r+")
	settings = yaml.load(yamlFile, Loader=yaml.SafeLoader)
	yamlFile.close()
except:
	print('create settings.yml')
	sys.exit(0)
def exe(arr):
	subprocess.run(arr, check=True)
def clean():
	try :
		exe(['conda','remove', '-y', '-n', str(settings["env_name"]) , '--all'])
		f = open('packages.yml', 'w')
		f.write('')
		f.close()
		print('\n\nclean succes')
	except:
		print('\n\nerror')
def install():
	try :
		exe(['conda', 'env', 'create', '-f', '=packages.yml'])
		print('\n\nsucces install')
	except:
		print('\n\nerror')
def save():
	try :
		res = subprocess.check_output('conda env export -n ' + str(settings["env_name"]) )
		f = open('packages.yml', 'w')
		f.write(res.decode('utf8'))
		f.close()
		print('\n\nsave env succes')
	except:
		print('\n\nerror')
def init():
		clean()
		exe(['conda', 'create', '-y', '-n', str(settings["env_name"]), 'python=' + str(settings["python_version"]), 'anaconda'])
		save()
		print('\n\nsucces init')
def run():
	try :
		exe(['python', settings["main_source"], settings["args"]])
		print('\n\nbuild succes')
	except:
		print('error')
def activate():
	try :
		exe(['activate', settings['env_name']])
	except:
		exe(['source','activate', settings['env_name']])
if __name__ == '__main__':
	args = sys.argv
	if len(args) <= 1:
		print('command:\ninstall or i: (package install name=dod),\nsave: (output env dod),\ninit: (create package.yml),\nrun: XD')
		sys.exit(0)
	if args[1] == 'install' or args[1] == 'i' :
		install()
	elif args[1] == 'save':
		save()
	elif args[1] == 'init':
		init()
	elif args[1] == 'clean':
		clean()
	elif args[1] == 'run':
		run()
